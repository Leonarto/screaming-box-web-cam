import React from "react";
import styles from "./Navigation.module.css";

class Navigation extends React.Component {
  static NAV_CONTROLS = "controls";
  static NAV_CAMERAS = "cameras";

  render() {
    const { onNav, active } = this.props;

    return (
      <div className={styles.main}>
        <button
          onClick={() => onNav(Navigation.NAV_CONTROLS)}
          className={active === Navigation.NAV_CONTROLS ? styles.active : ""}
        >
          Controls
        </button>
        <button
          onClick={() => onNav(Navigation.NAV_CAMERAS)}
          className={active === Navigation.NAV_CAMERAS ? styles.active : ""}
        >
          Cameras
        </button>
      </div>
    );
  }
}

export default Navigation;
