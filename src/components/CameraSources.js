import React from "react";
import axios from "axios";
import styles from "./CameraSources.module.css";

export default class CameraSources extends React.Component {
  state = {
    sources: [],
  };

  componentDidMount() {
    axios
      .get("http://runningios.com/screamingbox/cameras.json")
      .then((res) => this.setState({ sources: res.data }));
  }

  handleRef = (ref) => {
    this.camera = ref;
  };

  render() {
    return (
      <div className={styles.main}>
        {this.state.sources.map((source) => (
          <div
            key={source.name}
            className={styles.source}
            onClick={() => window.WebCam.setSource(source.source)}
          >
            {source.name}
          </div>
        ))}
      </div>
    );
  }
}
