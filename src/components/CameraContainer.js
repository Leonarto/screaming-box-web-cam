import React from "react";
import styles from "./CameraContainer.module.css";

export default class CameraContainer extends React.Component {
  componentDidMount() {
    this.camera.appendChild(window.WebCam.getCameraNode());
  }

  handleRef = (ref) => {
    this.camera = ref;
  };

  render() {
    return (
      <div className={styles.main}>
        <div className={styles.camera} id="camera" ref={this.handleRef} />
      </div>
    );
  }
}
