import React from "react";
import styles from "./CameraControls.module.css";

export default class CameraControls extends React.Component {
  componentWillUnmount() {
    window.removeEventListener("mouseup", this.endMoveCam);
  }

  moveCamEvent = (e) => {
    window.WebCam.move(
      this.position.x - e.clientX,
      e.clientY - this.position.y
    );
    this.position = { x: e.clientX, y: e.clientY };
  };

  endMoveCam = () =>
    this.controls.removeEventListener("mousemove", this.moveCamEvent);

  handleMouseDown = (e) => {
    if (this.controls) {
      this.position = { x: e.clientX, y: e.clientY };
      this.controls.addEventListener("mousemove", this.moveCamEvent);
      window.addEventListener("mouseup", this.endMoveCam);
    }
  };

  handleRef = (ref) => {
    this.controls = ref;
  };

  render() {
    return (
      <div className={styles.main}>
        <label>
          <strong>Click in circle & drag</strong>
        </label>
        <div
          className={styles.control}
          onMouseDown={this.handleMouseDown}
          ref={this.handleRef}
        />
      </div>
    );
  }
}
