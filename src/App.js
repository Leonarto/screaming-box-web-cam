import React, { useState } from "react";
import "./App.css";
import CameraSources from "./components/CameraSources";
import Navigation from "./components/Navigation";
import CameraContainer from "./components/CameraContainer";
import CameraControls from "./components/CameraControls";

function App() {
  const [currentNav, setCurrentNav] = useState(Navigation.NAV_CAMERAS);

  return (
    <div className="App">
      <div className="content-wrapper">
        <CameraContainer />
        <div className="second-column">
          <Navigation onNav={setCurrentNav} active={currentNav} />
          {currentNav === Navigation.NAV_CAMERAS && <CameraSources />}
          {currentNav === Navigation.NAV_CONTROLS && <CameraControls />}
        </div>
      </div>
    </div>
  );
}

export default App;
